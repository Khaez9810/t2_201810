package model.data_structures;
import java.util.*;

public class Iterador<T extends Comparable<T>> implements Iterator<T> {

	private Node<T> iter;	
	
	public Iterador(Node<T> current) {
		iter = current;	
		// TODO Auto-generated constructor stub
	}
			
	public Node<T> getIter() {
		return iter;
	}

	public void setIter(Node<T> iter) {
		this.iter = iter;
	}

	@Override
	public boolean hasNext() {
		return iter.getNext()!=null;
		// TODO Auto-generated method stub
	}

	@Override
	public T next() {
		if(iter.getNext()!=null) {
		iter = iter.getNext();
		T value = iter.getValue();
		return value;
		}else return null;
	}
	
	public T prev() {
		if(iter.getPrev()!=null) {
			iter = iter.getPrev();
			T value = iter.getValue();
			return value;
			}else return null;
	}
	
	public T getValue() {
		return iter.getValue();
	}
	

}
