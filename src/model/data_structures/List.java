package model.data_structures;


public class List<T extends Comparable<T>> implements LinkedList<T> {

	private Node<T> head;
	private Node<T> tail;
	private Iterador<T> current;
	private int size;

	public List() {
		head = tail = null;
		current = new Iterador<T>(null);
		size = 0;
	}

	public Node<T> getHead() {
		return head;
	}

	public Node<T> getTail() {
		return tail;
	}

	public Iterador<T> getIterador(){
		return current;
	}

	@Override
	public T getCurrent() {
		return current.getValue();
	}

	public int getSize() {
		return size;
	}


	@Override
	public boolean add(T item) {
		Node<T> add = new Node<T>();
		add.setValue(item);
		boolean var = false;
		if(head==null) {
			head = tail = add;
			current.setIter(tail);
			size++;
			var= !var;
		}
		else {
			tail.setNext(add);
			tail = add;
			current.setIter(tail);
			size++;
			var=!var;
		}
		return var;
	}

	@Override
	public boolean delete(T item) {
		boolean var = false;
		//Borrar el primer elemento
		if(head.getValue()==item) {
			head = head.getNext();
			current.setIter(head);
			size--;
			var=!var;
		}
		else{
			//Borra cualquier elemento
			current.setIter(head);
			boolean var2 = false;
			Node<T> aux;
			while(!var2 && current.hasNext()) {
				aux = current.getIter();
				if(aux.getNext().getValue()==item) 
					var2=!var2;
				else current.next();

			}
			aux = current.getIter();
			aux.setNext(aux.getNext().getNext());
			size--;
			var=!var;
		}
		return var;
	}

	@Override
	public T get(T item) {
		if(head.getValue()==item) {
			current.setIter(head);
			return head.getValue();
		}
		else if(tail.getValue()==item) {
			current.setIter(tail);
			return tail.getValue();
		}
		else{
			current.setIter(head);
			Node<T> aux = current.getIter();
			while(aux.getValue()!=item && current.hasNext()) {
				current.next();
				aux = current.getIter();
			}
			return (aux!=tail)?aux.getValue():null;
		}
	}

	@Override
	public T getAt(int pos) {
		if(pos>size) {
			return null;
		}else {
			int var = 0;
			current.setIter(head);
			while(var< pos && current.hasNext()) {
				current.next();
				var++;
			}
			return current.getValue();
		}
	}

	@Override
	public void listing() {
		Node<T> aux= current.getIter() ,aux2 ;
		current.setIter(head);
		boolean var = false;
		while(current.hasNext() && !var) {
			aux2 = current.getIter();
			if(aux2.getNext()==aux)
				var=!var;
			else current.next(); 
		}
		current.getIter().setNext(null);
		tail.setNext(head);
		tail = current.getIter();
		head = aux;
		current.setIter(head);
	}

	@Override
	public T next() {
		return current.next();
	}


}
