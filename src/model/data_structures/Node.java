package model.data_structures;

public class Node <T extends Comparable<T>> {

	private Node<T> next;
	private Node<T> prev;
	private T value;

	public Node(){
		this.next = null;
		this.prev = null;
		this.value= null;
	}


	public Node<T> getNext() {
		return next;
	}


	public void setNext(Node<T> next) {
		this.next = next;
	}


	public Node<T> getPrev() {
		return prev;
	}


	public void setPrev(Node<T> prev) {
		this.prev = prev;
	}


	public T getValue() {
		return value;
	}


	public void setValue(T value) {
		this.value = value;
	}
	

}