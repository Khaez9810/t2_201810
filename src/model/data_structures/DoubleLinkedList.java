package model.data_structures;

public class DoubleLinkedList<T extends Comparable<T>> implements LinkedList<T> {

	private Node<T> head;
	private Node<T> tail;
	private Iterador<T> current;
	private int size;

	public DoubleLinkedList() {
		head = tail = null;
		current = new Iterador<T>(null);
		size=0;
		// TODO Auto-generated constructor stub
	}

	public Node<T> getHead() {
		return head;
	}


	public Node<T> getTail() {
		return tail;
	}
	
	public Iterador<T> getIterador(){
		return current;
	}

	@Override
	public boolean add(T item) {
		Node<T> add = new Node<T>();
		add.setValue(item);
		boolean var=false;
		if(head==null) {
			head = tail = add;
			var=!var;
			size++;
			current.setIter(head);
		}else if(!exist(item)){
			add.setPrev(tail);
			tail.setNext(add);
			tail = add;
			var=!var;
			size++;
			current.setIter(tail);
		}
		current.setIter(tail);
		return var;
	}

	@Override
	public boolean delete(T item) {
		boolean var = false;
		if(item==head.getValue()) {
			if(head.getNext()==null) {
				head = null;
			}else {
			head.getNext().setPrev(null);
			head = head.getNext();
			current.setIter(head);
			}
			size--;
			var =!var;
		}else{
			//Borra cualquier elemento
			current.setIter(head);
			boolean var2 = false;
			Node<T> aux,aux2;
			while(!var2 && current.hasNext()) {
				aux = current.getIter();
				if(aux.getNext().getValue()==item) 
					var2=!var2;
				else current.next();

			}
			aux = current.getIter();
			aux2 = aux.getNext().getNext();
			aux.setNext(aux2);
			aux2.setPrev(aux);
			size--;
			var=!var;
		}
		return var;
	}

	@Override
	public T get(T item) {
		if(head.getValue()==item) {
			current.setIter(head);
			return head.getValue();
		}
		else if(tail.getValue()==item) {
			current.setIter(tail);
			return tail.getValue();
		}
		else{
			current.setIter(head);
			Node<T> aux = current.getIter();
			while(aux.getValue()!=item && current.hasNext()) {
				current.next();
				aux = current.getIter();
			}
			return (aux!=tail)?aux.getValue():null;
		}
	}


	@Override
	public T getAt(int pos) {
		if(pos>size) {
			return null;
		}else {
			int var = 0;
			current.setIter(head);
			while(var< pos && current.hasNext()) {
				current.next();
				var++;
			}
			return current.getValue();
		}
	}

	@Override
	public int getSize() {
		return size;
	}

	@Override
	public void listing() {
		Node<T> aux = current.getIter();
		current.prev(); Node<T> aux2 = current.getIter();
		aux.setPrev(null);
		aux2.setNext(null);
		tail.setNext(head);
		head.setPrev(tail);
		head = aux; tail = aux2;
		current.setIter(aux);
	}

	@Override
	public T getCurrent() {
		return current.getValue();
		
	}

	@Override
	public T next() {
		return current.next();
	}
	
	public T prev() {
		return current.prev();
	}
	
	
	private boolean exist(T item){
		boolean var = false;
		current.setIter(head);
		while(current.hasNext() && !var){
			Node<T> aux = current.getIter();
			if(aux.getValue().compareTo(item)==0){
				var=!var;
			} else current.next();
		}
		return var;
	}
	
}
