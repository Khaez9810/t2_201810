package model.data_structures;

/**
 * Abstract Data Type for a linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add: add a new element T 
 * delete: delete the given element T 
 * get: get the given element T (null if it doesn't exist in the list)
 * size: return the the number of elements
 * get: get an element T by position (the first position has the value 0) 
 * listing: set the listing of elements at the first element
 * getCurrent: return the current element T in the listing (return null if it doesn�t exists)
 * next: advance to next element in the listing (return if it exists)
 * @param <T>
 */
public interface LinkedList <T extends Comparable<T>>  {
	
	/**
	 *Metodo para agregar un elemento a la lista
	 *@param Node, nodo que se va a agregar a la lista
	 *@returns True si se pudo agregar, false en caso contrario 
	 */
	public boolean add(T item);
	
	/**
	 *Metodo para eliminar un elemento de la lista
	 *@param Node, nodo que se va a eliminar de la lista
	 *@returns True si se pudo eliminar, false en caso contrario 
	 */
	public boolean delete(T item);
	
	/**
	 * Metodo para buscar un nodo 
	 * @param Node, nodo que se busca dentro de la lista
	 * @return Nodo si se encuentra dentro de la lista, null si no existe en la lista
	 */
	public T get(T item);
	
	/**
	 * Metodo que retorna el tama�o de la lista
	 * @return un entero que representa el tam�ano de la lista
	 */
	public int getSize();
	
	
	/**
	 * Metodo para buscar un nodo en un indice
	 * @param Pos, posicion del nodo en la lista
	 * @return Nodo si se encuentra dentro de la lista, null si no existe en la lista
	 */
	public T getAt(int pos);
	
	
	/**
	 * Metodo para enlistar los nodos
	 * @pos: El primer nodo de la lista se le asigna a el inicial
	 */
	public void listing();
	
	/**
	 * Metodo que retorna el nodo actual
	 * @return El nodo actual
	 */
	public T getCurrent();
	
	/**
	 * Metodo que retorna el nodo siguiebte
	 * @return el nodo siguiente
	 */
	public T next();
	

}
