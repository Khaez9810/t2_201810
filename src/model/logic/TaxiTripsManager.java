package model.logic;

import api.ITaxiTripsManager;
import model.vo.Taxi;
import model.vo.Service;
import model.data_structures.DoubleLinkedList;
import model.data_structures.Iterador;
import model.data_structures.LinkedList;
import model.data_structures.List;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Iterator;

import com.google.gson.*;


public class TaxiTripsManager implements ITaxiTripsManager {

	private List<Taxi> ListaTaxis = new List<Taxi>();
	private List<Service> ListaServicios = new List<Service>();
	
	// TODO
	// Definition of data model 
	
	public void loadServices (String serviceFile) {
		JsonParser parser = new JsonParser();
		
		String company="",taxiid="",tripid="";
		double trip_miles=0,trip_total=0; int trip_seconds=0,dropof_comunity_area=0;
		try {
			JsonArray array = (JsonArray) parser.parse(new FileReader(serviceFile));
			int size = array.size();
			for (int i = 0; i < size; i++) {
				JsonObject ob = (JsonObject) array.get(i);
				
				
				//Si el atributo no existe se declara como N/A o 0.
				company = ob.get("company")!=null?ob.get("company").getAsString():"N/A";
				dropof_comunity_area = ob.get("dropoff_community_area") != null?ob.get("dropoff_community_area").getAsInt():0;
				taxiid = ob.get("taxi_id")!=null?ob.get("taxi_id").getAsString():"N/A";
				trip_miles = ob.get("trip_miles")!=null?ob.get("trip_miles").getAsDouble():0;
				trip_total = ob.get("trip_total")!=null?ob.get("trip_total").getAsDouble():0;
				taxiid = ob.get("taxi_id")!=null?ob.get("taxi_id").getAsString():"N/A";
				tripid = ob.get("trip_id")!=null?ob.get("trip_id").getAsString():"N/A";
				
				Taxi addT = new Taxi(taxiid, company);
				Service addS = new Service(tripid, taxiid, trip_seconds, trip_miles, trip_total, dropof_comunity_area);
				
				ListaTaxis.add(addT);
				ListaServicios.add(addS);	
			}
		} catch (JsonIOException e) {
			// TODO: handle exception
		
		} catch (JsonParseException e) {
			// TODO: handle exception
		}catch (FileNotFoundException e) {
			// TODO: handle exception
		}
		
		
		// TODO Auto-generated method stub
		System.out.println("Se ha cargado el archivo: " + serviceFile);
	}

	@Override
	public LinkedList<Taxi> getTaxisOfCompany(String company) {
		LinkedList<Taxi> ListaCompania = new DoubleLinkedList<Taxi>();
		for (int i = 0; i < ListaTaxis.getSize(); i++) {
			if(ListaTaxis.getAt(i).getCompany().equals(company)){
				ListaCompania.add(ListaTaxis.getAt(i));
			}
		}
		
		System.out.println("Se ha creado la lista de la siguiente compania: " + company);
		return ListaCompania;
	}

	@Override
	public LinkedList<Service> getTaxiServicesToCommunityArea(int communityArea) {
		LinkedList<Service> ListaArea = new List<Service>();
		for (int i = 0; i < ListaServicios.getSize(); i++) {
			if(ListaServicios.getAt(i).getComunityArea()==communityArea){
				ListaArea.add(ListaServicios.getAt(i));
			}
		}
				
		System.out.println("Se ha creado la lista con respecto a una area especifica " + communityArea);
		return ListaArea;
	}


}
