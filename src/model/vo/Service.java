package model.vo;

/**
 * Representation of a Service object
 */
public class Service implements Comparable<Service> {

	//atributos objeto servicio

	private String tripID;

	private String taxiID;

	private int tripSeconds;

	private double tripMiles;

	private double tripTotal;

	private int comunityArea;



	public Service (String TripID,String TaxiD, int tripSeconds, double tripMiles, double tripTotal, int comunityArea){
		this.tripID = TripID;
		this.taxiID = TaxiD;
		this.tripSeconds = tripSeconds;
		this.tripMiles = tripMiles;
		this.tripTotal = tripTotal;
		this.comunityArea = comunityArea;

	}

	/**
	 * @return id - Trip_id
	 */
	public String getTripId() {
		// TODO Auto-generated method stub
		return tripID;
	}	

	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return taxiID;
	}	

	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		// TODO Auto-generated method stub
		return tripSeconds;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		// TODO Auto-generated method stub
		return tripMiles;
	}

	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {
		// TODO Auto-generated method stub
		return getTripTotal();
	}

	public int getComunityArea(){
		return comunityArea;
	}

	@Override
	public int compareTo(Service o) {
		// TODO Auto-generated method stub
		return Integer.compare(comunityArea, o.comunityArea);
	}
}
