package model.vo;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>{

	//Atributos objeto taxi
	
	private String taxiID;
	
	private String company;
	
	
	
	public Taxi(String taxiID, String company){
		this.taxiID=taxiID;
		this.company=company;
	}
	
	
	
	/**
	 * @return id - taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return taxiID;
	}

	/**
	 * @return company
	 */
	public String getCompany() {
		// TODO Auto-generated method stub
		return company;
	}
	
	@Override
	public int compareTo(Taxi o) {
		return taxiID.compareTo(o.taxiID);
		
	}	
}
