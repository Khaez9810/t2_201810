package test;


import static org.junit.Assert.assertEquals;

import junit.framework.TestCase;
import model.data_structures.DoubleLinkedList;


public class DoubleLinkedListTest extends TestCase{

	private DoubleLinkedList<Integer> Lista;

	private void setupEscenario1() 
	{
		Lista = new DoubleLinkedList<Integer>();
	}

	private void setupEscenario2() 
	{
		setupEscenario1();

		Lista.add(1);
		Lista.add(2);
		Lista.add(3);
		Lista.add(4);
		Lista.add(5);
		Lista.add(6);
		Lista.add(7);
		Lista.add(8);
	}

	public void testDoubleLinkedList() {
		setupEscenario1();
		assertEquals("No se inicializo correctamente el iterador",true,Lista.getIterador()!=null);
		assertEquals("No se inicializo la lista",0,Lista.getSize());

		setupEscenario2();
		assertEquals("No se inicializo correctamente el iterador",Lista.getTail(),Lista.getIterador().getIter());
		assertEquals("No se inicializo la lista",8,Lista.getSize());
	}


	public void testGetHeadTailSizeCurrentNextAndPrev() {
		//Prieba con lsita vacia
		setupEscenario1();
		assertEquals("No se tiene el tamanio esperado",0, Lista.getSize());
		assertEquals("No se tiene la cabeza correcta", null, Lista.getHead());
		assertEquals("No se tiene la cola correcta", null, Lista.getTail());

		//Pruebas con lista no vacia
		setupEscenario2();
		assertEquals("No se tiene el tamanio esperado",8, Lista.getSize());
		assertSame("No se tiene la cabeza correcta", 1, Lista.getHead().getValue());
		assertSame("No se tiene la cola correcta", 8, Lista.getTail().getValue());

		//Pruebas Get Current
		Lista.get(3);
		assertSame("El iterador no esta funcionando correctamente",3,Lista.getCurrent());
		Lista.getAt(10);
		assertSame("El iterador no esta funcionando correctamente",3,Lista.getCurrent());
		Lista.get(10);
		assertSame("El iterador no esta funcionando correctamente",8,Lista.getCurrent());

		//Prueba del next
		assertNull("Deberia ser nula la respuesta", Lista.next());
		Lista.get(7);
		assertNotNull("No deberia ser nula la respuesta",Lista.next());
		assertSame("El valor no es el esperado", 8,Lista.getCurrent());

		//Prubea del prev
		Lista.getAt(0);
		assertNull("Deberia ser nula la respuesta",Lista.prev());
		Lista.get(4);
		assertNotNull("No deberia ser nula la respuesta",Lista.prev());
		assertSame("El valor no es el esperado",3,Lista.getCurrent());

	}

	public void testGetAndGetAt() {
		setupEscenario2();
		//Pruebas Get
		assertNull("El elemento no deberia existir en la lista", Lista.get(10));
		assertNotNull("El elemento deberia existir en la lista", Lista.get(5));
		assertSame("El elemento no es el correcto",5,Lista.get(5));
		for (int i = 1; i <= Lista.getSize(); i++) {
			assertSame("La busqueda no funciona",i, Lista.get(i));
			assertSame("El iterador no esta funcionando correctamente,",i,Lista.getCurrent());
		}

		//Pruebas Get At
		assertNotNull("El elemento deberia existir en la lista", Lista.getAt(5));
		assertNull("El elemento no deberia existir en la lista", Lista.getAt(10));
		assertSame("El elemento no es el correcto",6,Lista.getAt(5));
		for (int i = 0; i < Lista.getSize(); i++) {
			assertSame("La busqueda no funciona",i+1, Lista.getAt(i));
			assertSame("El iterador no esta funcionando correctamente,",i+1,Lista.getCurrent());
		}

	}

	public void testAddandDelete() {
		//Prueba agregando a una lsita vacia
		setupEscenario1();
		assertTrue("No se agrego el elemento a la lista", Lista.add(20));
		assertTrue("No se agrego el elemento a la lista", Lista.add(30));
		assertFalse("No se debio agregar el elemento a la lista", Lista.add(20));
		assertTrue("No se elimino el elemento a la lista", Lista.delete(20));
		assertNotNull("Se perdio la referencia a la cabeza",Lista.getHead());
		assertNotNull("Se perdio la referencia a la cola",Lista.getTail());
		assertSame("El elemeto en la cabeza no es el correcto",30,Lista.getHead().getValue());
		Lista.delete(30);
		assertNull("No deberian existir elementos en la lista",Lista.getHead());

		//Prueba agregando a una lista con datos
		setupEscenario2();
		assertTrue("No se agrego el elemento a la lista", Lista.add(20));
		assertTrue("No se agrego el elemento a la lista", Lista.add(30));
		assertTrue("No se elimino el elemento a la lista", Lista.delete(20));
		assertNotNull("Se perdio la referencia a la cabeza",Lista.getHead());
		assertNotNull("Se perdio la referencia a la cola",Lista.getTail());
		assertTrue("No se elimino el elemento",Lista.delete(5));
		assertNull("No se elimino el elemento correcto", Lista.get(5));
	}

	public void testListing() {
		setupEscenario2();
		Lista.get(4);
		Lista.listing();
		assertSame("El iterador no esta funcionando correctamente", 4,Lista.getCurrent());
		//Se verifica una mitad de la lsita
		for (int i = 0; i < Lista.getSize()-3; i++) {
			assertSame("La lista no fue reordenada",i+4,Lista.getAt(i));
		}
		//Se verifica la otra mitad
		for (int i = 5; i < Lista.getSize(); i++) {
			assertSame("La lista no fue reordenada",i-4 , Lista.getAt(i));
		}
		//Se verifica que el elemento actual no halla cambiado
		Lista.get(4);
		assertSame("El iterador no esta funcionando correctamente",4,Lista.getCurrent());

		//Se verifica cuando el elemento es el ultimo
		setupEscenario2();
		Lista.get(8);
		Lista.listing();
		assertSame("El iterador no esta funcionando correctamente", 8,Lista.getCurrent());
		assertSame("La lista no fue reordenada",8,Lista.getAt(0));
		for (int i = 1; i < Lista.getSize(); i++) {
			assertSame("La lista no fue reordenada",i,Lista.getAt(i));
		}

	}
}
